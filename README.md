**Lilyum Nedir?** \
\
Lilyum, OpenSUSE tabanlı bir Linux dağıtımıdır. Daha fazla bilgi almak şu linkleri ziyaret edebilirsiniz: \
https://gitlab.com/linuxlilyum/lilyum \
https://lilyum.org \
\
**Bu depo ne işe yarar** \
\
Bu depo, Linux Lilyum için kullanılan .src.rpm paketlerini barındırır. Bu paketler, sisteminize kurulmaya müsait olan . *mimari* .rpm paketlerinin hazırlanması için kaynak oluşturur. Bu paketlerin ikili hâli için alttaki linke bakabilirsiniz.\
https://build.opensuse.org/project/show/home:Tarbetu \
Ayrıca, bu paketlerin derlendiği depoyu OpenSUSE sisteminizde şöyle ekleyebilirsiniz: \
* sudo zypper ar -f https://download.opensuse.org/repositories/home:/Tarbetu/OpenSUSE_Tumbleweed/ *lilyum*
